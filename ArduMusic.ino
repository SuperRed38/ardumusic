/*
  ArduMusic
  SuperRed38
*/
#include <Arduboy.h>
Arduboy arduboy;

void setup() {
  // put your setup code here, to run once:
  arduboy.begin();
  arduboy.clear();
  arduboy.print("ArduMusic");
  arduboy.setCursor(0, 10);
  arduboy.print("Coded by SuperRed38");
  arduboy.setCursor(0, 20);
  arduboy.print("Press the keys");
  arduboy.display();
}

void loop() {
  // put your main code here, to run repeatedly:
  if (arduboy.pressed(LEFT_BUTTON) == true ) {
    arduboy.setRGBled(255, 0, 0);
    arduboy.tunes.tone(1000, 1000);
    delay(10);
    arduboy.tunes.tone(1025, 1025);
    delay(10);
  }
  if (arduboy.pressed(UP_BUTTON) == true ) {
    arduboy.setRGBled(255, 0, 0);
    arduboy.tunes.tone(1100, 1100);
    delay(10);
    arduboy.tunes.tone(1125, 1125);
    delay(10);
  }
  if (arduboy.pressed(RIGHT_BUTTON) == true ) {
    arduboy.setRGBled(255, 0, 0);
    arduboy.tunes.tone(1200, 1200);
    delay(10);
    arduboy.tunes.tone(1225, 1225);
    delay(10);
  }
  if (arduboy.pressed(DOWN_BUTTON) == true ) {
    arduboy.setRGBled(255, 0, 0);
    arduboy.tunes.tone(900, 900);
    delay(10);
    arduboy.tunes.tone(925, 925);
    delay(10);
  }
  if (arduboy.pressed(A_BUTTON) == true ) {
    arduboy.setRGBled(255, 0, 0);
    arduboy.tunes.tone(1300, 1300);
    delay(10);
    arduboy.tunes.tone(1325, 1325);
    delay(10);
  }
  if (arduboy.pressed(B_BUTTON) == true ) {
    arduboy.setRGBled(255, 0, 0);
    arduboy.tunes.tone(1400, 1400);
    delay(10);
    arduboy.tunes.tone(1425, 1425);
    delay(10);
  }
  arduboy.setRGBled(0, 0, 0);
  arduboy.tunes.tone(0, 0);
}